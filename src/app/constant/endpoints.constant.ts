export class EndpointsConstant {
  static usersWithPagination = 'http://localhost:8080/users';
  static usersAll = 'http://localhost:8080/users/all';
  static vehicles = 'http://localhost:8080/cars';
  static contracts = 'http://localhost:8080/contracts';
  static contractsOfUser = 'http://localhost:8080/contracts/users';
}
