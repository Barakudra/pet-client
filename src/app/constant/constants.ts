export const Constants = {
  "url404": "http://webexpert.com.ua/wp-content/uploads/2016/07/404-page.jpg",
  "required": "required",
  "minlength": "minlength",
  "hidePassword": "Hide password",
  "visibilityOn": "visibility",
  "visibilityOff": "visibility_off",
  "userWasCreated": "userWasCreated",
  "userWasUpdated": "userWasUpdated",
  "vehicleWasDeleted": "vehicleWasDeleted",
  "kitWasAddToContract": "kitWasAddToContract",
  "contractWasCreated": "contractWasCreated",
  "vehicleWasCreated": "vehicleWasCreated",
  "errorSmthWrong": "errorSmthWrong",
  "paramEmail": "email",
  "paramLastName": "lastName"
}
