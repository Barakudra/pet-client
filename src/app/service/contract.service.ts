import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

import {EndpointsConstant} from "../constant/endpoints.constant";
import {InsuranceContractModel} from "../model/insurance-contract.model";
import {ClientDetailsModel} from "../model/client-details.model";

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  constructor(private http: HttpClient) {
  }

  registrationContract(insuranceContractModel: InsuranceContractModel): Observable<InsuranceContractModel> {
    return this.http.post<InsuranceContractModel>(EndpointsConstant.contracts, insuranceContractModel);
  }

  getContractsOfUser(client: ClientDetailsModel): Observable<Array<InsuranceContractModel>> {
    return this.http.post<Array<InsuranceContractModel>>(`${EndpointsConstant.contractsOfUser}/${client.id}`, client);
  }
}
