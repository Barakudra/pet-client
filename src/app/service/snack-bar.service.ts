import {SnackBarInfoComponent} from "../component/snack-bar-info/snack-bar-info.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) {
  }

  openSnackBar(): void {
    this.snackBar.openFromComponent(SnackBarInfoComponent, {
      duration: 2000,
    });
  }
}
