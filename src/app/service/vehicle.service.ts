import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

import {VehicleDetailsModel} from "../model/vehicle-details.model";
import {EndpointsConstant} from "../constant/endpoints.constant";
import {VehicleBasicModel} from "../model/vehicle-basic.model";

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private http: HttpClient) {
  }

  getAllBasicVehicleModels(): Observable<Array<VehicleBasicModel>> {
    return this.http.get<Array<VehicleBasicModel>>(EndpointsConstant.vehicles);
  }

  getVehicleDetails(id: number): Observable<VehicleDetailsModel> {
    return this.http.get<VehicleDetailsModel>(`${EndpointsConstant.vehicles}/${id}`);
  }

  registrationVehicle(vehicle: VehicleDetailsModel): Observable<VehicleDetailsModel> {
    return this.http.post<VehicleDetailsModel>(EndpointsConstant.vehicles, vehicle);
  }

  deleteVehicle(id: number): Observable<VehicleDetailsModel> {
    return this.http.delete<VehicleDetailsModel>(`${EndpointsConstant.vehicles}/${id}`);
  }
}
