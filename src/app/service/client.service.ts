import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

import {ClientDetailsModel} from "../model/client-details.model";
import {EndpointsConstant} from "../constant/endpoints.constant";
import {ClientBasicModel} from "../model/client-basic.model";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
  }

  getAllClients(): Observable<Array<ClientBasicModel>> {
    return this.http.get<Array<ClientBasicModel>>(EndpointsConstant.usersAll);
  }

  getAllClientsPageable(page: number, size: number, sort: string, direction: string, filter: string): Observable<any> {
    return this.http.get<any>(`${EndpointsConstant.usersWithPagination}?page=${page}&size=${size}&sort=${sort},${direction}&filter=${filter}`);
  }

  getClientDetails(id: number): Observable<ClientDetailsModel> {
    return this.http.get<ClientDetailsModel>(`${EndpointsConstant.usersWithPagination}/${id}`);
  }

  registrationClient(client: ClientDetailsModel): Observable<ClientDetailsModel> {
    return this.http.post<ClientDetailsModel>(EndpointsConstant.usersWithPagination, client);
  }

  updateClient(client: ClientDetailsModel): Observable<ClientDetailsModel> {
    return this.http.put<ClientDetailsModel>(EndpointsConstant.usersWithPagination + '/' + client.id, client);
  }
}
