import {Injectable} from "@angular/core";
import {InsuranceKitModel} from "../model/insurance-kit.model";
import {VehicleBasicModel} from "../model/vehicle-basic.model";
import {ClientDetailsModel} from "../model/client-details.model";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public insuranceKits: Array<InsuranceKitModel> = [];
  public vehicles: Array<VehicleBasicModel> = [];
  public desiredClient!: ClientDetailsModel;
  public snackBarInfoMessage!: string;
}
