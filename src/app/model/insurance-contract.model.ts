import {InsuranceKitModel} from "./insurance-kit.model";
import {VehicleDetailsModel} from "./vehicle-details.model";

export interface InsuranceContractModel {
  id: number,
  insuranceKits: Array<InsuranceKitModel>,
  vehicle: VehicleDetailsModel,
  createdAt: Date,
  updatedAt: Date
}
