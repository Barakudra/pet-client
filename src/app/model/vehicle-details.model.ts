import {ClientDetailsModel} from "./client-details.model";

export interface VehicleDetailsModel {
  id: number,
  createdAt: Date,
  updatedAt: Date,
  color: string,
  engineCapacity: number,
  yearOfManufacture: Date,
  weight: number,
  registerNumber: string,
  owner: ClientDetailsModel
}
