export interface VehicleBasicModel {
    id: number,
    registerNumber: string,
    isContractExist: boolean,
}
