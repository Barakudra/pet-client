export interface PartOfVehicleModel {
  id: number,
  vehiclePart: string
}
