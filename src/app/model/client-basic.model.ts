export interface ClientBasicModel {
  id: number,
  email: string,
  lastName: String
}
