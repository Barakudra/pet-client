import {PartOfVehicleModel} from "./part-of-vehicle.model";

export interface InsuranceKitModel {
  id: number,
  createdAt: Date,
  updatedAt: Date,
  duration: Date,
  compensationPercent: number,
  damageLevel: string,
  partsOfVehicle: Array<PartOfVehicleModel>
}
