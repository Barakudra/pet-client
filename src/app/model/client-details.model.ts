export interface ClientDetailsModel {
  id: number,
  email: string,
  password: string,
  birthDate: Date,
  firstName: string,
  lastName: String,
  createdAt: Date,
  updatedAt: Date
}
