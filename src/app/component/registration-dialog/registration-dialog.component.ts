import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {MatDialogRef} from "@angular/material/dialog";

import {ClientService} from "../../service/client.service";
import {ClientDetailsModel} from "../../model/client-details.model";
import {Constants} from "../../constant/constants";
import {DataService} from "../../service/data.service";
import {SnackBarService} from "../../service/snack-bar.service";

@Component({
  selector: 'app-registration-dialog',
  templateUrl: './registration-dialog.component.html',
  styleUrls: ['./registration-dialog.component.css']
})
export class RegistrationDialogComponent implements OnInit, OnDestroy {
  registerForm!: FormGroup;
  email!: FormControl;
  firstName!: FormControl;
  lastName!: FormControl;
  password!: FormControl;
  birthDate!: FormControl;
  hidePassword = true;

  private subscriptions: Subscription[] = [];

  constructor(private clientService: ClientService,
              private dataService: DataService,
              private snackBarService: SnackBarService,
              public dialogRef: MatDialogRef<RegistrationDialogComponent>,) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  submitForm(): void {
    this.subscriptions.push(this.clientService.registrationClient(this.buildRequest()).subscribe(
      (response) => {
        this.dataService.snackBarInfoMessage = Constants.userWasCreated;
        this.snackBarService.openSnackBar();
      },
      (error) => {
        if (error.status === 409) {
          this.dataService.snackBarInfoMessage = error.error;
        } else {
          this.dataService.snackBarInfoMessage = Constants.errorSmthWrong;
        }
        this.snackBarService.openSnackBar();
      }
    ));
  }

  changePasswordVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }

  private initForm(): void {
    this.email = new FormControl(this.email, Validators.compose([Validators.required, Validators.email]));
    this.firstName = new FormControl(this.firstName, Validators.compose([Validators.required, Validators.minLength(4)]));
    this.lastName = new FormControl(this.lastName, Validators.compose([Validators.required, Validators.minLength(4)]));
    this.password = new FormControl(this.password, Validators.compose([Validators.required, Validators.minLength(8)]));
    this.birthDate = new FormControl(this.birthDate, Validators.compose([Validators.required]));

    this.registerForm = new FormGroup({
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      password: this.password,
      birthDate: this.birthDate
    });
  }

  private buildRequest(): ClientDetailsModel {
    return {
      id: 0,
      email: this.email.value,
      password: this.password.value,
      birthDate: this.birthDate.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  }

  private closeDialog(): void {
    this.dialogRef.close();
  }
}
