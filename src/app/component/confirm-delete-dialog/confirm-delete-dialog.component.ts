import {Component, Inject, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from "rxjs";

import {VehicleService} from "../../service/vehicle.service";
import {DataService} from "../../service/data.service";
import {Constants} from "../../constant/constants";
import {SnackBarService} from "../../service/snack-bar.service";

@Component({
  selector: 'app-confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.component.html',
  styleUrls: ['./confirm-delete-dialog.component.css']
})
export class ConfirmDeleteDialogComponent implements OnDestroy {

  private subscriptions: Subscription[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: { idDeleted: number },
              private vehicleService: VehicleService,
              private dataService: DataService,
              public dialogRef: MatDialogRef<ConfirmDeleteDialogComponent>,
              private snackBarService: SnackBarService) {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  deleteVehicle(): void {
    this.subscriptions.push(this.vehicleService.deleteVehicle(this.data.idDeleted).subscribe(
      (response) => {
        this.dataService.snackBarInfoMessage = Constants.vehicleWasDeleted;
        this.snackBarService.openSnackBar();
        this.dataService.vehicles = this.dataService.vehicles.filter(veh => veh.id != this.data.idDeleted);
        this.dialogRef.close();
      },
      (error) => {
        if (error.status === 400) {
          this.dataService.snackBarInfoMessage = error.error;
        } else {
          this.dataService.snackBarInfoMessage = Constants.errorSmthWrong;
        }
        this.snackBarService.openSnackBar();
      }
    ));
  }

  closeConfirmDialog(): void {
    this.dialogRef.close();
  }
}
