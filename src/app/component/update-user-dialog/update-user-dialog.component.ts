import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from "../../service/data.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatDialogRef} from "@angular/material/dialog";

import {ClientService} from "../../service/client.service";
import {ClientDetailsModel} from "../../model/client-details.model";
import {Constants} from "../../constant/constants";
import {SnackBarService} from "../../service/snack-bar.service";

@Component({
  selector: 'app-update-user-dialog',
  templateUrl: './update-user-dialog.component.html',
  styleUrls: ['./update-user-dialog.component.css']
})
export class UpdateUserDialogComponent implements OnInit, OnDestroy {

  registerForm!: FormGroup;
  firstName!: FormControl;
  lastName!: FormControl;
  birthDate!: FormControl;
  private subscriptions: Subscription[] = [];

  updatedUser = this.dataService.desiredClient;

  constructor(private dataService: DataService,
              private clientService: ClientService,
              private _snackBar: MatSnackBar,
              private snackBarService: SnackBarService,
              public dialogRef: MatDialogRef<UpdateUserDialogComponent>,) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  submitForm(): void {
    this.subscriptions.push(this.clientService.updateClient(this.updateClient()).subscribe(
      (response) => {
        this.dataService.snackBarInfoMessage = Constants.userWasUpdated;
        this.snackBarService.openSnackBar();
        this.closeDialog();
      },
      (error) => {
        this.dataService.snackBarInfoMessage = Constants.errorSmthWrong;
        this.snackBarService.openSnackBar();
      }
    ));
  }

  private initForm(): void {
    this.firstName = new FormControl(this.firstName, Validators.compose([Validators.required, Validators.minLength(4)]));
    this.lastName = new FormControl(this.lastName, Validators.compose([Validators.required, Validators.minLength(4)]));
    this.birthDate = new FormControl(this.birthDate, Validators.compose([Validators.required]));

    this.registerForm = new FormGroup({
      firstName: this.firstName,
      lastName: this.lastName,
      birthDate: this.birthDate
    });

    this.firstName.setValue(this.updatedUser.firstName);
    this.lastName.setValue(this.updatedUser.lastName);
    this.birthDate.setValue(this.updatedUser.birthDate);
  }

  private updateClient(): ClientDetailsModel {
    this.updatedUser.firstName = this.firstName.value;
    this.updatedUser.lastName = this.lastName.value;
    this.updatedUser.birthDate = this.birthDate.value;
    return this.updatedUser;
  }

  private closeDialog(): void {
    this.dialogRef.close();
  }
}
