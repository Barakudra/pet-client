import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {MatDialog} from "@angular/material/dialog";

import {ClientService} from "../../../service/client.service";
import {ClientDetailsModel} from "../../../model/client-details.model";
import {ContractService} from "../../../service/contract.service";
import {DataService} from "../../../service/data.service";
import {UpdateUserDialogComponent} from "../../update-user-dialog/update-user-dialog.component";

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit, OnDestroy {

  client!: ClientDetailsModel;
  private subscriptions: Subscription[] = [];
  private id!: number;

  constructor(private clientService: ClientService,
              private contractService: ContractService,
              private activateRouter: ActivatedRoute,
              private router: Router,
              private dataService: DataService,
              private dialog: MatDialog,) {
  }

  ngOnInit(): void {
    this.subscriptions.push(this.activateRouter.params.subscribe(params => {
      this.id = params['id'];
    }));

    this.subscriptions.push(this.clientService.getClientDetails(this.id).subscribe(
      data => {
        this.client = data;
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }

  showContractsOfClient(): void {
    this.router.navigate(['/contracts/users', this.client.id]);
    this.dataService.desiredClient = this.client;
  }

  showClientUpdateDialog(): void {
    this.dataService.desiredClient = this.client;
    this.dialog.open(UpdateUserDialogComponent, {
      height: '600px',
      width: '400px',
    });
  }
}
