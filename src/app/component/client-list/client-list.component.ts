import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {MatSort, Sort} from "@angular/material/sort";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";

import {ClientService} from "../../service/client.service";
import {ClientBasicModel} from "../../model/client-basic.model";
import {Constants} from "../../constant/constants";

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit, OnDestroy {

  emailParam = Constants.paramEmail;
  lastNameParam = Constants.paramLastName;

  sortParam = '';
  direction = '';
  filter = '';

  currentPage = 0;
  pageSize = 10;
  totalElements!: number;
  pager!: any;

  public dataSource!: MatTableDataSource<ClientBasicModel>;
  displayedColumns = ['email', 'lastName'];
  clients!: Array<ClientBasicModel>;

  private subscription!: Subscription;

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private clientService: ClientService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getClientsBasicModelWithPagination(new PageEvent());
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  showClientDetails(id: number): void {
    this.router.navigate(['/users', id]);
  }

  getClientsBasicModelWithPagination(event: PageEvent): void {
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.subscription = this.clientService.getAllClientsPageable(this.currentPage, this.pageSize, this.sortParam, this.direction, this.filter).subscribe(
      data => {
        this.clients = data.content;
        this.totalElements = data.totalElements;
        this.currentPage = data.number;
        this.pageSize = data.size;
        this.dataSource = new MatTableDataSource(this.clients);
      }
    );
  }

  setDirection(event: Sort): void {
    this.direction = event.direction;
    this.getClientsBasicModelWithPagination(new PageEvent());
  }

  setSort(param: string): void {
    this.sortParam = param;
    this.getClientsBasicModelWithPagination(new PageEvent());
  }

  filtering(event: any): void {
    this.filter = event.target.value;
    this.getClientsBasicModelWithPagination(new PageEvent());
  }

  clearInput() {
    this.filter = '';
    this.getClientsBasicModelWithPagination(new PageEvent());
  }
}
