import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from "@angular/material/dialog";

import {ClientService} from "../../service/client.service";
import {RegistrationDialogComponent} from "../registration-dialog/registration-dialog.component";
import {AddCarDialogComponent} from "../add-car-dialog/add-car-dialog.component";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {

  constructor(
    private clientService: ClientService,
    private router: Router,
    private dialog: MatDialog,
  ) {
  }

  openClientList(): void {
    this.router.navigate(['/users']);
  }

  openVehicleList(): void {
    this.router.navigate(['/cars']);
  }

  openRegistrationClientDialog(): void {
    this.dialog.open(RegistrationDialogComponent, {
      height: '500px',
      width: '400px',
    });
  }

  openRegistrationVehicleDialog(): void {
    this.dialog.open(AddCarDialogComponent, {
      height: '600px',
      width: '400px',
    });
  }

  openCreateContractPage(): void {
    this.router.navigate(['/contracts']);
  }
}
