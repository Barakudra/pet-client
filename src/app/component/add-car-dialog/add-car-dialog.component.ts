import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatSelectChange} from "@angular/material/select";
import {Subscription} from "rxjs";
import {MatDialogRef} from "@angular/material/dialog";

import {ClientService} from "../../service/client.service";
import {VehicleService} from "../../service/vehicle.service";
import {ClientBasicModel} from "../../model/client-basic.model";
import {VehicleDetailsModel} from "../../model/vehicle-details.model";
import {ClientDetailsModel} from "../../model/client-details.model";
import {Constants} from "../../constant/constants";
import {DataService} from "../../service/data.service";
import {SnackBarService} from "../../service/snack-bar.service";

@Component({
  selector: 'app-add-car-dialog',
  templateUrl: './add-car-dialog.component.html',
  styleUrls: ['./add-car-dialog.component.css']
})
export class AddCarDialogComponent implements OnInit, OnDestroy {

  registerForm!: FormGroup;
  color!: FormControl;
  engineCapacity!: FormControl;
  yearOfManufacture!: FormControl;
  weight!: FormControl;
  registrationNumber!: FormControl;

  owners: Array<ClientBasicModel> = [];
  idOwner!: number;
  ownerDetails!: ClientDetailsModel;
  private subscriptions: Subscription[] = [];

  constructor(private vehicleService: VehicleService,
              private clientService: ClientService,
              private snackBarService: SnackBarService,
              private dataService: DataService,
              public dialogRef: MatDialogRef<AddCarDialogComponent>) {
  }

  ngOnInit(): void {
    this.subscriptions.push(this.clientService.getAllClients().subscribe(
      data => {
        this.owners = data;
      },
      error => {
        this.dataService.snackBarInfoMessage = Constants.errorSmthWrong;
        this.snackBarService.openSnackBar();
      }
    ))
    this.initForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  submitForm(): void {
    this.subscriptions.push(this.vehicleService.registrationVehicle(this.buildRequest()).subscribe(
      (response) => {
        this.dataService.snackBarInfoMessage = Constants.vehicleWasCreated;
        this.snackBarService.openSnackBar();
        this.closeDialog()
      },
      (error) => {
        console.log(error)
        this.dataService.snackBarInfoMessage = Constants.errorSmthWrong;
        this.snackBarService.openSnackBar();
      }
    ));
  }

  getUserDetailsById(event: MatSelectChange): void {
    this.idOwner = event.value;
    this.clientService.getClientDetails(this.idOwner).subscribe(
      data => {
        this.ownerDetails = data;
      }
    )
  }

  private initForm(): void {
    this.color = new FormControl(this.color, Validators.compose([Validators.required]));
    this.engineCapacity = new FormControl(this.engineCapacity, Validators.compose([Validators.required]));
    this.yearOfManufacture = new FormControl(this.yearOfManufacture, Validators.compose([Validators.required]));
    this.weight = new FormControl(this.weight, Validators.compose([Validators.required]));
    this.registrationNumber = new FormControl(this.registrationNumber, Validators.compose([Validators.required]));

    this.registerForm = new FormGroup({
      color: this.color,
      engineCapacity: this.engineCapacity,
      yearOfManufacture: this.yearOfManufacture,
      weight: this.weight,
      registrationNumber: this.registrationNumber
    });
  }

  private buildRequest(): VehicleDetailsModel {
    return {
      id: 0,
      color: this.color.value,
      engineCapacity: this.engineCapacity.value,
      yearOfManufacture: this.yearOfManufacture.value,
      weight: this.weight.value,
      registerNumber: this.registrationNumber.value,
      owner: this.ownerDetails,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  }

  private closeDialog(): void {
    this.dialogRef.close();
  }
}
