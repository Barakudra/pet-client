import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {MatDialog} from "@angular/material/dialog";

import {VehicleService} from "../../service/vehicle.service";
import {VehicleBasicModel} from "../../model/vehicle-basic.model";
import {ConfirmDeleteDialogComponent} from "../confirm-delete-dialog/confirm-delete-dialog.component";
import {DataService} from "../../service/data.service";

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit, OnDestroy {

  vehicles!: Array<VehicleBasicModel>;
  private subscription!: Subscription;

  constructor(private vehicleService: VehicleService,
              private router: Router,
              private dialog: MatDialog,
              public dataService: DataService) {
  }

  ngOnInit(): void {
    this.subscription = this.vehicleService.getAllBasicVehicleModels().subscribe(
      data => {
        this.dataService.vehicles = data;
      }
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openDeleteDialog(id: number): void {
    this.dialog.open(ConfirmDeleteDialogComponent, {
      height: '200px',
      width: '400px',
      data: {
        idDeleted: id
      }
    });
  }

  showVehicleDetails(id: number): void {
    this.router.navigate(['/cars', id]);
  }
}
