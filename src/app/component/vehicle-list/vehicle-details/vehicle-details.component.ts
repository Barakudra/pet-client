import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";

import {VehicleService} from "../../../service/vehicle.service";
import {VehicleDetailsModel} from "../../../model/vehicle-details.model";

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.css']
})
export class VehicleDetailsComponent implements OnInit, OnDestroy {

  vehicle!: VehicleDetailsModel;
  private subscription!: Subscription;
  private id!: number;

  constructor(private vehicleService: VehicleService,
              private router: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.id = this.router.snapshot.params.id;
    this.subscription = this.vehicleService.getVehicleDetails(this.id).subscribe(
      data => {
        this.vehicle = data;
      }
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
