import {Component, OnInit} from '@angular/core';

import {DataService} from "../../service/data.service";

@Component({
  selector: 'app-snack-bar-info',
  templateUrl: './snack-bar-info.component.html',
  styleUrls: ['./snack-bar-info.component.css']
})
export class SnackBarInfoComponent implements OnInit {
  infoMessage!: string;

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.infoMessage = this.dataService.snackBarInfoMessage;
  }
}
