import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup, Validators} from "@angular/forms";

import {DamageLevel} from "../../../enum/damage-level";
import {PartOfVehicles} from "../../../enum/part-of-vehicles";
import {InsuranceKitModel} from "../../../model/insurance-kit.model";
import {DataService} from "../../../service/data.service";
import {PartOfVehicleModel} from "../../../model/part-of-vehicle.model";
import {Constants} from "../../../constant/constants";
import {SnackBarService} from "../../../service/snack-bar.service";

@Component({
  selector: 'app-create-kit-dialog',
  templateUrl: './create-kit-dialog.component.html',
  styleUrls: ['./create-kit-dialog.component.css']
})
export class CreateKitDialogComponent implements OnInit {

  damageLevels = Object.values(DamageLevel);
  partsOfVehicle = Object.values(PartOfVehicles);

  hidden = true;

  createKitForm!: FormGroup;
  durationControl!: FormControl;
  percentControl!: FormControl;
  damageLevelControl!: FormControl;
  partOfVehiclesControl!: FormControl;

  constructor(public dialogRef: MatDialogRef<CreateKitDialogComponent>,
              private dataService: DataService,
              private snackBarService: SnackBarService) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  addInsuranceKit(): void {
    this.dataService.insuranceKits.push(this.buildInsuranceKit());
    this.dataService.snackBarInfoMessage = Constants.kitWasAddToContract;
    this.snackBarService.openSnackBar();
    this.dialogRef.close();
  }

  private initForm(): void {
    this.durationControl = new FormControl(this.durationControl, Validators.compose([Validators.required]));
    this.percentControl = new FormControl(this.percentControl, Validators.compose([Validators.required]));
    this.damageLevelControl = new FormControl(this.damageLevelControl);
    this.partOfVehiclesControl = new FormControl(this.partOfVehiclesControl);

    this.createKitForm = new FormGroup({
      durationControl: this.durationControl,
      percentControl: this.percentControl,
      damageLevelControl: this.damageLevelControl,
      partOfVehiclesControl: this.partOfVehiclesControl
    });
  }

  private buildInsuranceKit(): InsuranceKitModel {
    this.buildPartsOfVehicle(this.partOfVehiclesControl.value);
    return {
      id: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
      duration: this.durationControl.value,
      compensationPercent: this.percentControl.value,
      damageLevel: this.damageLevelControl.value,
      partsOfVehicle: this.buildPartsOfVehicle(this.partOfVehiclesControl.value)
    }
  }

  private buildPartsOfVehicle(stringsNameOfPart: Array<string>): Array<PartOfVehicleModel> {
    const partsOfVehicle: PartOfVehicleModel[] = [];
    stringsNameOfPart.forEach(
      currentValue => {
        partsOfVehicle.push(this.createPartOfVehicleObject(currentValue))
      }
    )
    return partsOfVehicle;
  }

  private createPartOfVehicleObject(part: string): PartOfVehicleModel {
    return {
      id: 0,
      vehiclePart: part
    }
  }
}
