import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {FormControl, FormGroup} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {MatSelectChange} from "@angular/material/select";
import {Router} from "@angular/router";

import {CreateKitDialogComponent} from "./create-kit-dialog/create-kit-dialog.component";
import {InsuranceKitModel} from "../../model/insurance-kit.model";
import {DataService} from "../../service/data.service";
import {VehicleDetailsModel} from "../../model/vehicle-details.model";
import {InsuranceContractModel} from "../../model/insurance-contract.model";
import {ContractService} from "../../service/contract.service";
import {PartOfVehicles} from "../../enum/part-of-vehicles";
import {VehicleService} from "../../service/vehicle.service";
import {VehicleBasicModel} from "../../model/vehicle-basic.model";
import {Constants} from "../../constant/constants";
import {SnackBarService} from "../../service/snack-bar.service";

@Component({
  selector: 'app-create-contract',
  templateUrl: './create-contract.component.html',
  styleUrls: ['./create-contract.component.css']
})
export class CreateContractComponent implements OnInit, OnDestroy {

  vehicles!: Array<VehicleBasicModel>;
  registerForm!: FormGroup;
  vehicleControl!: FormControl;
  isDisabled = true;

  chosenBasicVehicle!: VehicleBasicModel;
  chosenVehicleDetails!: VehicleDetailsModel;

  partsOfVehicle = Object.values(PartOfVehicles);
  chosenParts: PartOfVehicles[] = [];

  private subscriptions: Subscription[] = [];

  constructor(private vehicleService: VehicleService,
              private dialog: MatDialog,
              private dataService: DataService,
              private contractService: ContractService,
              private snackBarService: SnackBarService,
              private router: Router,) {
  }

  nestedInsuranceKits: Array<InsuranceKitModel> = this.dataService.insuranceKits;

  ngOnInit(): void {
    this.subscriptions.push(this.vehicleService.getAllBasicVehicleModels().subscribe(
      data => {
        this.vehicles = data;
      }, error => {
        this.dataService.snackBarInfoMessage = Constants.errorSmthWrong;
        this.snackBarService.openSnackBar();
      }
    ));
    this.initForm();
    this.chosenParts = this.partsOfVehicle.filter(result => result.includes(this.nestedInsuranceKits.toString()));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  openRCreateKitDialog(): void {
    this.dialog.open(CreateKitDialogComponent, {
      height: '500px',
      width: '400px',
    });
  }

  registrationContract(): void {
    this.subscriptions.push(this.contractService.registrationContract(this.fillInsuranceContract()).subscribe(
      (response) => {
        this.dataService.snackBarInfoMessage = Constants.contractWasCreated;
        this.snackBarService.openSnackBar();
        this.openMainComponent();
      },
      (error) => {
        this.dataService.snackBarInfoMessage = Constants.errorSmthWrong;
        this.snackBarService.openSnackBar();
      }
    ));
  }

  isButtonShow(event: MatSelectChange): void {
    this.chosenBasicVehicle = event.value;
    this.isDisabled = !(event.value != undefined && !this.chosenBasicVehicle.isContractExist);
  }

  getVehicleById(id: number): void {
    this.subscriptions.push(this.vehicleService.getVehicleDetails(id).subscribe(
      data => {
        this.chosenVehicleDetails = data;
      }
    ))
  }

  isCreateButtonDisable(): boolean {
    return this.nestedInsuranceKits.length == 0;
  }

  deleteInsuranceKit(kit: InsuranceKitModel): void {
    console.log(kit)
    this.dataService.insuranceKits.forEach((element, index) => {
      if (element == kit) {
        this.dataService.insuranceKits.splice(index, 1);
      }
      console.log(this.dataService.insuranceKits)
    })
  }

  private initForm(): void {
    this.vehicleControl = new FormControl(this.vehicleControl);

    this.registerForm = new FormGroup({
      vehicle: this.vehicleControl
    });
  }

  private fillInsuranceContract(): InsuranceContractModel {
    return {
      id: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
      vehicle: this.chosenVehicleDetails,
      insuranceKits: this.nestedInsuranceKits
    }
  }

  private openMainComponent(): void {
    this.router.navigate(['']);
  }
}
