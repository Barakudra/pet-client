import {Component} from '@angular/core';

import {Constants} from "../../constant/constants";

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent {
  url404 = Constants.url404;
}
