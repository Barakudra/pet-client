import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  constructor(private translateService: TranslateService) {
    this.translateService.setDefaultLang('en');
  }

  setLanguage(languageCode: string): void {
    this.translateService.use(languageCode);
  }
}
