import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";

import {ContractService} from "../../service/contract.service";
import {DataService} from "../../service/data.service";
import {InsuranceContractModel} from "../../model/insurance-contract.model";

@Component({
  selector: 'app-contract-of-user',
  templateUrl: './contract-of-user.component.html',
  styleUrls: ['./contract-of-user.component.css']
})
export class ContractOfUserComponent implements OnInit, OnDestroy {
  listOfContracts!: Array<InsuranceContractModel>;
  private subscriptions: Subscription[] = [];

  constructor(private contractService: ContractService,
              private dataService: DataService) {
  }

  ngOnInit(): void {
    const desiredClient = this.dataService.desiredClient;
    this.subscriptions.push(this.contractService.getContractsOfUser(desiredClient).subscribe(
      data => {
        this.listOfContracts = data;
      }
    ));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
