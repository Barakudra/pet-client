import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MatSliderModule} from '@angular/material/slider';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from "@angular/material/divider";
import {RouterModule, Routes} from '@angular/router';
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {MatToolbarModule} from "@angular/material/toolbar";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatDialogModule} from "@angular/material/dialog";
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatOptionModule} from "@angular/material/core";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatChipsModule} from "@angular/material/chips";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {NgxPaginationModule} from "ngx-pagination";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";

import {ClientListComponent} from './component/client-list/client-list.component';
import {MainComponent} from './component/main/main.component';
import {VehicleListComponent} from './component/vehicle-list/vehicle-list.component';
import {NotFoundComponent} from './component/not-found/not-found.component';
import {AppComponent} from './app.component';
import {ToolbarComponent} from './component/toolbar/toolbar.component';
import {RegistrationDialogComponent} from './component/registration-dialog/registration-dialog.component';
import {AddCarDialogComponent} from './component/add-car-dialog/add-car-dialog.component';
import {VehicleDetailsComponent} from './component/vehicle-list/vehicle-details/vehicle-details.component';
import {ClientDetailsComponent} from './component/client-list/client-details/client-details.component';
import {CreateContractComponent} from './component/create-contract/create-contract.component';
import {CreateKitDialogComponent} from './component/create-contract/create-kit-dialog/create-kit-dialog.component';
import {ConfirmDeleteDialogComponent} from './component/confirm-delete-dialog/confirm-delete-dialog.component';
import {ContractOfUserComponent} from './component/contract-of-user/contract-of-user.component';
import { UpdateUserDialogComponent } from './component/update-user-dialog/update-user-dialog.component';
import { SnackBarInfoComponent } from './component/snack-bar-info/snack-bar-info.component';

const appRoutes: Routes = [
  {path: '', component: MainComponent},
  {path: 'users', component: ClientListComponent},
  {path: 'users/:id', component: ClientDetailsComponent},
  {path: 'cars', component: VehicleListComponent},
  {path: 'cars/:id', component: VehicleDetailsComponent},
  {path: 'contracts', component: CreateContractComponent},
  {path: 'contracts/users/:id', component: ContractOfUserComponent},
  {path: '**', component: NotFoundComponent}
];

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ClientListComponent,
    MainComponent,
    VehicleListComponent,
    NotFoundComponent,
    ToolbarComponent,
    RegistrationDialogComponent,
    AddCarDialogComponent,
    VehicleDetailsComponent,
    ClientDetailsComponent,
    CreateContractComponent,
    CreateKitDialogComponent,
    ConfirmDeleteDialogComponent,
    ContractOfUserComponent,
    UpdateUserDialogComponent,
    SnackBarInfoComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    RouterModule.forRoot(appRoutes),
    MatButtonModule,
    MatDividerModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatToolbarModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    MatCardModule,
    MatSelectModule,
    FormsModule,
    MatOptionModule,
    MatDatepickerModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    NgxPaginationModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
